#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

int velicina_spremnika;
int* spremnik;

int prebroji_prazne()
{
	int i, prazni = 0;

	for (i = 0; i < velicina_spremnika; i++) {
		if (spremnik[i] == 0)
			prazni++;
	}

	return prazni;
}

void preslozi(int* poc_rupe)
{
	int i, j;

	for (i = 0; i < velicina_spremnika; i++) {
		if (spremnik[i] == 0) {
			for (j = i+1; j < velicina_spremnika; j++) {
				if (spremnik[j] != 0) {
					spremnik[i] = spremnik[j];
					spremnik[j] = 0;
					break;
				}	
			}
		}

	}

	for (i = 0; i < velicina_spremnika; i++) {
		if (spremnik[i] == 0)
			break;	
	}

	*poc_rupe = i;
}

void ispis_sprem()
{
	int i;

	for (i = 0; i < velicina_spremnika; i++)
		printf("%3d",(1+i)%10);
	printf("\n");
	
	
	for (i = 0; i < velicina_spremnika; i++)
		printf("%3d",spremnik[i]);
	printf("\n");
}


void upisi(int pocetak_rupe, int br_trazenih_lok, int tek_z)
{
	int j;

	//printf("upisi %d %d %d\n",  pocetak_rupe,  br_trazenih_lok, tek_z);

	for (j = pocetak_rupe; j < pocetak_rupe+br_trazenih_lok; j++)
		spremnik[j] = tek_z;
}


int lociraj_rupe(int* poc_vel_rupa)
{
	int i, poc_rupe, velicina_rupe, br_rupe = 0;

	for (i = 0; i < velicina_spremnika; i++) {

		if (spremnik[i] != 0) continue;

		poc_rupe = i;
		velicina_rupe = 0;

		while (spremnik[i] == 0 && i < velicina_spremnika) {
			i++;
			velicina_rupe++;
		}
		
		poc_vel_rupa[2*br_rupe] = poc_rupe;
		poc_vel_rupa[2*br_rupe+1] = velicina_rupe;
		br_rupe++;
	}

	return br_rupe;
}

void ispisi_rupe(int* poc_vel_rupa, int br_rupe)
{
	int i;

	for (i = 0; i < br_rupe; i++)
		printf("rupa%d: poc = %d, vel = %d\n", i, poc_vel_rupa[2*i], poc_vel_rupa[2*i+1]);
}

int nadji_min_sirinu_rupe(int* poc_vel_rupa, int br_rupe, int br_trazenih_lok, int* poc_min_rupe)
{
	int i, min_sirina_rupe = velicina_spremnika + 1;

	for (i = 0; i < br_rupe; i++) {
		if (poc_vel_rupa[2*i+1] >= br_trazenih_lok && poc_vel_rupa[2*i+1] < min_sirina_rupe) {
			min_sirina_rupe = poc_vel_rupa[2*i+1];
			*poc_min_rupe =poc_vel_rupa[2*i];
		}
	}

	return min_sirina_rupe;
}

void zahtjev(int tek_z, int br_trazenih_lok)
{
	int br_rupe, min_sirina_rupe, poc_min_rupe, poc_rupe_nakon_preslag;
	int* poc_vel_rupa = (int*)malloc(velicina_spremnika*sizeof(int));
	if (poc_vel_rupa == NULL) {
		printf("greska kod init polja\n");
		exit(1);
	}

	printf("zahtjev %d %d\n", tek_z, br_trazenih_lok);

	if (prebroji_prazne() < br_trazenih_lok) {
		printf("Zahtjev se ne može poslužiti - nema dovoljno slobodnih lokacija.\n");
		return;
	}

	br_rupe = lociraj_rupe(poc_vel_rupa);

	ispisi_rupe(poc_vel_rupa, br_rupe);

	min_sirina_rupe = nadji_min_sirinu_rupe(poc_vel_rupa, br_rupe, br_trazenih_lok, &poc_min_rupe);

	//printf("min %d\n",min_sirina_rupe);
	if (min_sirina_rupe != velicina_spremnika+1) {
		upisi(poc_min_rupe, br_trazenih_lok, tek_z);		
	} else {
		printf("Preslagivanje spremnika...\n");
		preslozi(&poc_rupe_nakon_preslag);
		printf("Gotovo preslagivanje spremnika.\n");
		upisi(poc_rupe_nakon_preslag, br_trazenih_lok, tek_z);
	}

	free(poc_vel_rupa);
}
 
void obrisi(int tek_z)
{
	int i;

	for (i = 0; i < velicina_spremnika; i++) {
		if (spremnik[i] == tek_z) {
			spremnik[i] = 0;
		}
	}
}

void kraj(int sig)
{
	printf("Kraj programa\n");
	exit(0);
}

int main(int argc, char *argv[])
{
	int i, z, c, zo, p;

	velicina_spremnika = atoi(argv[1]);

	if (velicina_spremnika <= 0) {
		printf("Velicina spremnika mora biti pozitivan broj!\n");
		exit(1);
	}

	spremnik = (int*) malloc(velicina_spremnika*sizeof(int));
	if (spremnik == NULL) {
		printf("Neuspjela rezervacija memorije!\n");	
		exit(1);
	}


	for (i = 0; i < velicina_spremnika; i++) {
		spremnik[i] = 0;
	}

	ispis_sprem();

	sigset(SIGINT, kraj);

	srand((unsigned) time(NULL));


	z = 0;
	while (1) {
		do {
			c = getchar();
		} while (c != 'o' && c != 'z' && c != 'p');
		

		if (c == 'z') {
			z++;
			zahtjev(z, 1+rand() % velicina_spremnika);
		} else if (c == 'o') {
			if (z == 0) {
				printf("Nema zahtjeva za osloboditi.\n");
				continue;
			}
			
			
			do {
				printf("Koji zahtjev treba osloboditi? z = %d\n", z);
				scanf("%d", &zo);
			} while (zo < 1);

			obrisi(zo);
		} else if (c == 'p'){
			preslozi(&p);
		}

		ispis_sprem();
	}

	free(spremnik);

	return 0;
}
